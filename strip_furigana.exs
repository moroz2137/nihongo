#! /usr/bin/env elixir

if length(System.argv()) != 1 do
  raise "No filename specified or too many arguments"
end

case System.cmd("which", ["pandoc"]) do
  {_, 0} -> :ok
  _ -> raise "Pandoc not found on the system"
end

[input_file] = System.argv()

text = File.read!(input_file)

output_file = (input_file |> Path.rootname()) <> "_no_furigana"
latex_path = output_file <> ".tex"
IO.puts("Writing stripped to #{latex_path}")

md_output = output_file <> ".md"

pattern = ~r/\\ruby\{([^\}]+)\}\{[^\}]+\}/

subbed = String.replace(text, pattern, "\\1")
File.write(latex_path, subbed)

IO.puts("Converting LaTeX to Markdown...")

System.cmd("pandoc", [latex_path, "-o", md_output])
